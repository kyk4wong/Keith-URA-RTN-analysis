## MATLAB Scripts for RTN analysis

This repository contains a library of MATLAB scripts written for RTN analysis.

For confidentiality reasons, Data is not stored in this repository. Please place data in the folder containing
the repository in the following structure: `data/(extractor-name)/*.csv`

### Running the Extractors

To run an extractor, open MATLAB in the directory containing this repository. Then, run the desired extractor.
Extractors all follow the naming convention `(Data_folder_name)_extractor.m`.

### Library Structure

The code is split into two sections: extractors and processors.
Processors run processes on the extracted data, such as the creation of charts and graphs.
Extractors read the data, transform it into a specific format, and then run the desired set of processors.
The extractor should place the all data into objects.
Each object represents a single test run.
The structure of the object is as follows:

```
object.temp (numeric)       (Kelvin)  : The temperature at which the test was conducted.
object.Vg   (numeric)       (Volts)   : The gate voltage at which the test was conducted.
object.Vd   (numeric)       (Volts)   : The drain voltage at which the test was conducted.
object.Id   (numeric array) (Amperes) : An array of drain currents that were taken at each sampling time.
object.time (numeric array) (seconds) : An array of sampling times.
```

Data should be placed in its own folder, in the following structure: `Data/(extractor-name)/*.csv`

Extractors should have the following naming convention: `(data-folder-name)_extractor.m`


