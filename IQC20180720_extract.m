%Extract data from IQC20180720 NMOS
% Format: Col1 = time4K  Col2 = Id4K  Col3 = time13K Col4 = Id13K
%         Col5 = time56K Col6 = Id56K Col7 = timeRT  Col8 = IdRT
clc
clear

%Define Vg, Vd, temp because they are not in the data
%Array are values for {4K, 13K, 56K, RT}
temps = {4, 13, 56, 300};
Vg = {0.4, 0.4, 0.5, 0.25};
Vd = {0.0025, 0.005, 0.005, 0.01};

% Define columns for data in the .csv
time_col = 1;
Id_col = 2;

% Define number of bins to use for each figure
nbins = {100,100,100,100};

% Define starting values to use for fitting
fit_values = {{[10,10], [10,10]}, {[1,1], [1,1]}, {[0.5,0.5], [0.05,0.05]}, {[0.1,0.1], [0.1,0.1]}};

% Define number of Gaussians to use for each dataset
close all

objects = [];

%Read CSV file from row 2, and place data into an object
data = csvread('Data/IQC20180720/NMOS.csv', 1, 0); 

% Four sets of data in 1 csv
for i = [1,2,3,4]
   
    object.filename = 'NMOS.csv';
    
    %Temp, Vg, Vd extracted from cells defined above
    object.temp = temps{i};
    object.Vg = Vg{i};
    object.Vd = Vd{i};
    
    %time, Id extracted from CSV
    object.time = data(:, 2*(i-1) + time_col);
    object.Id = data(:, 2*(i-1) + Id_col);
    
    % 56K data has a huge section in the beginning with very high current
    % (10x other values), so remove it
    if(i == 3)
        object.time = object.time(4001:end);
        object.Id   = object.Id(4001:end);
    end
    
    %Clean zeros from the data
    object.time(object.time==0) = [];
    object.Id(object.Id==0) = [];
    
    objects = [objects object];
    
    figure_title = strcat(num2str(object.temp), 'K', num2str(object.Vd), 'Vd', num2str(object.Vg), 'Vg');
    means = extract_gaussians(figure_title, object.Id, 1000, 2);
    %plot_time_constant(figure_title, object.Id, object.temp, object.Vd, object.Vg, object.time, nbins{i}, fit_values{i});
end