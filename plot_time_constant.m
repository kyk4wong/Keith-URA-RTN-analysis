% Generates a histogram of the amounts of time that the transistor trapped
% and emitted an electron. Also determines the curve of best fit, using the
% equation a*exp(-x/b). The coefficient 'b' is the time constant.
%
% NOTE: This script does not attempt to remove non-RTN sources of noise. It may be helpful
%       to pass the data through a high pass filter to remove 1/f noise before using this script.
% NOTE: Absolute values of currents are used to determine the high and low current states.
% Inputs:
%   fig_title       (string)        : Title of the figure.
%   Id              (numeric array) : The drain currents.
%   temp            (numeric)       : The temperature at which the measurments were taken.
%   Vd              (numeric)       : The drain voltage at which the measurments were taken.
%   Vg              (numeric)       : The gate voltage at which the measurments were taken.
%   sampling_time   (numeric array  : The times at which the drain current measurments were taken.
%   nbins           (numeric)       : The number of bins to use for the histogram.
%   fit_values      (cell array)    : A size 2x2 horizontal cell of arrays starting values to use
%                                     for fitting the exponential. The first pair of values is for
%                                     the high state. The second pair is for the low state.
function f = plot_time_constant(fig_title, Id, temp, Vd, Vg, sampling_time, nbins, fit_values)
    figure('Name', fig_title);
    % Input checking
    if (ischar(fig_title) == 0)
        throw(MExpection('plot_time_constant::TypeError', 'Variable "title" should be a char.'));
    end
    
    if (isnumeric(Id) == 0)
       throw(MException('extract_time_constant', 'Varaible "Id" should be a numeric array.'));
    end
    
    % Find average of drain currents to determine transitions
    % A drain current above the average is considered the 'high' state
    % A drain current below the average is considered the 'low' state
    Id = abs(Id);
    average = mean(Id);

	% Determine sampling period
	sampling_period = mean(diff(sampling_time));
    
    % Arrays will keep track of how long each state lasted for each time the state is entered
    high = [];
    low = [];
    
    % Initialize state using the first value in the array
    % state = 1 is high current state
    % state = 0 is low current state
    % count keeps track of how many sampling periods we have been in the same state
    if (Id(1) > average)
        state = 1;
    else
        state = 0;
    end
    count = 1;
    
    % Iterate through the remaining data points
    for i = 2:length(Id)
        
        % Transition from low state to high state detected
        if (state == 0 && Id(i) > average)
            low = [low count];
            state = 1;
            count = 0;
        end
        
        % Transition from high state to low state detected
        if (state == 1 && Id(i) < average)
            high = [high count];
            state = 0;
            count = 0;
        end
        
        count = count + 1;
    end
    
    % Insert the last point
    if (state == 0)
        low = [low count];
    else
        high = [high count];
    end
    
    
    dataset = {high, low}; % Put data for each state into a structure that can be iterated over
    states = {'high', 'low'}; % Labels for titles
    
    % Iterate through data extracted for each state
    for i = 1:2
        disp(fig_title);
        disp(states{i});
        % Place data pertaining to the amount of time the current was in the state into bins
        counts = histcounts(dataset{i}, nbins);
        %counts = counts(1:100);
        
        % Determine equation coefficients for the state
        % Equation is fitted to a-b*exp(-c*x)
        x = linspace(0, length(counts)-1, length(counts));
        decaying_exp = fittype('a*exp(-x/b)');
        equation = fit(x.', counts.', decaying_exp, 'StartPoint', fit_values{i});
        coeffvals = round(coeffvalues(equation),4);
        
        % Output equation to console
        console_out = strcat('state=', states{i}, ' T=', num2str(temp), ' Vd=', num2str(Vd), ' Vg=', num2str(Vg), ' sampling rate=', num2str(sampling_period));
        disp(console_out);
        disp(equation);
        
        % Plot histogram for the state and the fitted curve
        subplot(2,1,i);
        bar(x, counts, 'BaseValue', 0);
        hold on;
        plot(x,equation(x), 'r-');
        axis([0, length(counts), 0, max(counts)]*1.1)
        chart_title = strcat('Tau-', states{i}, ' a*exp(-x/b)', ' a=', num2str(coeffvals(1)), ' b=', num2str(coeffvals(2)));
        title(chart_title);
        hold off;
    end
end
