% Extract data from IBM
clc
clear

% Open files and extract file names for .csv data
filenames = dir('Data/RTN_SOI_0416/*.csv');
filenames = {filenames.name};
close all

objects = [];
Vd_col   = 1;
Vg_col   = 2;
time_col = 3;
Id_col   = 4;
Ig_col   = 5;

for ii = fliplr(1:length(filenames))
    filename = filenames{ii};
    object.filename = filename;
    
    % Extract the temperature from the filename
    strs = strsplit(filename,'_');
    str = cellstr(strs{2});
    index = strfind(str, 'K'); %extract index of first instance of "k"
    str = str{1};
    index = index{1};
    str = str(1:index-1);
    object.temp = str2num(str);

    % Extract data from CSV and place into the object
    data = csvread(strcat('Data/RTN_SOI_0416/',filename), 218, 1);
    object.Vg   = data(:,Vg_col);
    object.Id   = data(:,Id_col);
    object.Vd   = data(:,Vd_col);
    object.time = data(:,time_col);
    object.Ig   = data(:,Ig_col);

    objects = [objects object];

    if (object.temp == 5);
        figure_title = strcat(str, 'K', num2str(object.Vd(1)), 'Vd', num2str(object.Vg(1)), 'Vg');
        %lag_plot(object.Id, object.temp, object.Vd(1), object.Vg(1), 70); % Hazem's lag plot generator, not runnable on my version of MATLAB
        plot_time_constant(figure_title, object.Id, object.temp, object.Vd(1), object.Vg(1), object.time, 50, {[1,1], [10,10]});
    end
end