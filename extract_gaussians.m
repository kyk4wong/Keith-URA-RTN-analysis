% Generate histogram of drain current values and fit histogram to Gaussian mixture.
% Determines the RTN levels.
%
% NOTE: The scaling used to display the Gaussians on the same scale as the
%       histogram is for visual purposes only. The real mean and variance
%       ae displayed in the console.
% Inputs:
%   fig_title  (string)        : Title of the figure.
%   Id         (numeric array) : The drain currents.
%   nbins      (integer)       : The number of bins to use for the histogram.
%   ngaussians (integer)       : The number of gaussians to fit into the Gaussian mixture.
function gm_mu = extract_gaussians(fig_title, Id, nbins, ngaussians)
    figure('Name', fig_title);
    
    % Fit the Gaussian Mixture
    gm_dist = fitgmdist(Id, ngaussians);
    
    % Extract mean and variance
    gm_mu = gm_dist.mu;
    gm_variance = gm_dist.Sigma; %I don't know what it's saved in Sigma, becauase its actualy the variance
    
    % Used to find the factor to scale by
    counts = histcounts(Id, nbins);
    
    % Used to plot the Gaussians
    x = linspace(min(Id), max(Id), nbins);
    
    histogram(Id, nbins);
    hold on;
    display(fig_title);
    for i = 1:ngaussians
       display(strcat('Mean: ', num2str(gm_mu(i)), ' Std Deviation: ', num2str(gm_variance(i)^0.5)));
       % Factor to unnormalize the gaussian
       % Uses the mean and the range to find which index the mean is
       % referring to in the histogram, and multiply the pdf by that value
       % Also multiply by (2*pi*gm_variance)^0.5 to remove scaling factor
       percentage = (gm_mu(i)-min(Id))/(max(Id)-min(Id)); % Find how far along into the range of currents the mean for the gaussian is located
       normalizer = (2*pi*gm_variance(i))^0.5;            % Un-normalizing scaling factor
       index      = round(length(counts)*percentage);     % The index in the histogram of the location of the mean current for the gaussian
       
       % There is a chance that the index that is found actually has a
       % small amount or zero values, so take a small range around the index and use the largest value
       % NOTE: I feel like there's a better way to do this, but I'm not
       % sure. Maybe something to do with averaging? But then if we have a
       % large number of bins then it makes it difficult to eliminate
       % outliers with small values.
       min_index  = max(1, index - round(0.02*nbins));
       max_index  = min(length(counts), index+round(0.02*nbins));
       mean       = max(counts(min_index:max_index));
       
       % Take the maximum value found +-2% of the range around the mean and
       % use that to scale the gaussian to the plot
       scale = mean*normalizer;
       
       % Generate pdf and scale it
       p = pdf('Normal', x, gm_mu(i), gm_variance(i)^0.5)*scale;
       
       % plot the scaled gaussians
       plot(x, p);
    end
    hold off;
end