%Extract data from IQC lab
% Format: Col1 = time4K Col2 = Id4K Col3 = timeRT Col4 = Id4K
clc
clear

%Define Vg, Vd, temp because they are not in the data
Vg = -0.7;
Vd = {-0.006, -0.005}; %{4K, RT}
temps = {4, 300};

% Define columns for data in the .csv
time_col = 1;
Id_col = 2;

% Define number of bins to use for each figure
nbins = {60 100};

% Define starting values to use for fitting
fit_values = [10, 10];

% Define number of Gaussians to use for each dataset

filenames = dir('Data/IQCNaYoungKim/*.csv');
close all

objects = [];

% Two sets of data in 1 excel
for i = 1:2
    
    %Read CSV file from row 2, and place data into an object
    data = csvread(strcat('Data/IQCNaYoungKim/',filenames.name), 1, 0); 
    object.filename = filenames.name;
    
    %Temp, Vg, Vd extracted from cells defined above
    object.temp = temps{i};
    object.Vg = Vg;
    object.Vd = Vd{i};
    
    %time, Id extracted from CSV, 4K d;ata in cols 1, 2, RT data in cols 3, 4
    object.time = data(:, 2*(i-1) + time_col);
    object.Id = data(:, 2*(i-1) + Id_col);
    
    %Clean zeros from the data
    object.time(object.time==0) = [];
    object.Id(object.Id==0) = [];
    
    objects = [objects object];
    
    figure_title = strcat(num2str(object.temp), 'K', num2str(object.Vd), 'Vd', num2str(object.Vg), 'Vg');
    extract_gaussians(figure_title, object.Id, 100, 2);
    %lag_plot(object.Id, object.temp, object.Vd, object.Vg, 1000);
    %plot_time_constant(figure_title, object.Id, object.temp, object.Vd, object.Vg, object.time, nbins{i}, fit_values);
end